"""
Created on Tue Aug 14 21:39:20 2018

@author: ishaanmehra
"""
from random import randint
import sqlite3 
from password_generator_main import generate_password,create_user

path = 'user.db'  
conn = sqlite3.connect(path, timeout=10)
c = conn.cursor()

for x in range(0,10):
    fullname = create_user(path)
    generatedpwd = generate_password(randint(6, 12), randint(1, 4))
    
    
    c.execute("UPDATE users1 SET Password = ? WHERE FullName = ?", (generatedpwd, fullname))
    conn.commit()
    
conn.close()       
    
