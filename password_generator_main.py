from random import randint
import random  
import sqlite3  
import string
import requests

def generate_password(lengthpwd, complexitypwd):
    
    alphabet = random.choice(string.ascii_lowercase)
    ualphabet =random.choice(string.ascii_uppercase)
    special_char = random.choice(string.punctuation)
    digit=str(randint(0, 9))
    pw_length = lengthpwd   
    mypw = set("")
    finalpw=set("")
    

    if complexitypwd==1:
        mypw = ''.join(random.choices(string.ascii_lowercase, k=pw_length))
        
    if complexitypwd==2:
       mypw = ''.join(random.choices(string.ascii_lowercase + string.digits , k=pw_length-1))  +  digit
      
       
    if complexitypwd==3:
        mypw = ''.join(random.choices(string.ascii_lowercase + string.digits+string.ascii_uppercase , k=pw_length- 3)) + alphabet +  ualphabet  +  digit 
       
    if complexitypwd==4:
         mypw = ''.join(random.choices(string.ascii_lowercase + string.digits+string.ascii_uppercase + string.punctuation, k=pw_length- 4))+ special_char + alphabet +  ualphabet  +  digit 
        
    
    finalpw=''.join(random.sample(mypw,len(mypw)))
    
    return finalpw

    

def check_password_level(password):
    pwds =set(password)
    complexity_level = 0
    complexity1s = set(string.ascii_lowercase)
    pwd_length = len( pwds)
    complexity2s = set(string.digits+string.ascii_lowercase)
    complexity3s = set(string.digits+string.ascii_lowercase+string.ascii_uppercase)
    complexity4s = set(string.digits+string.ascii_lowercase+string.ascii_uppercase+string.punctuation)
    
    if (pwd_length >= 8 and pwds.issubset(complexity1s )) :  
        complexity_level = 2
    elif (pwd_length >= 8 and pwds.issubset(complexity2s )) :  
        complexity_level = 3      
    elif (pwd_length<8 and pwds.issubset(complexity1s)):
        complexity_level = 1    
    elif (pwd_length<8 and pwds.issubset(complexity2s )) :
        complexity_level = 2  
    elif pwds.issubset(complexity3s) :
        complexity_level = 3 
    elif pwds.issubset(complexity4s):
        complexity_level = 4      
  
    
    return complexity_level
    


def create_user(user):
    
    data = dict(requests.get("https://randomuser.me/api/?results=1&name=first&noinfo").json())
    
    fullname = data['results'][0]['name']['first']+" "+data['results'][0]['name']['last']
    email = data['results'][0]['email']
    table_c = '''CREATE TABLE IF NOT EXISTS users1(FullName TEXT, Email TEXT unique,Password TEXT)'''
    
    conn = sqlite3.connect(user, timeout=10)
    c = conn.cursor()
    c.execute(table_c)        
    c.execute('''INSERT INTO users1(FullName, Email) VALUES(?,?)''', (fullname,email))
    conn.commit()


    return fullname
    

