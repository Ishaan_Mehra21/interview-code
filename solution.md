The password generator contains three scripts-
Password_Generator_main
Password_Checker
User_password_generator

1.Password_Generator_main comprises of three functions generate_password,check_password_level and create_user.

a.The generate_password method takes complexity and length   as parameter and returns a string comprising of randomly 
  generated password.The password was generated depending on the complexity level that was passed as parameter.Then 
  random.choice function was used to generate password.Once all the the string was randomly selected then .join 
  was used to combine the string.In the end random.sample was used to shuffle the characters in the string.


b.The check_password_level has a string password as its parameter and returns a string named complexity level. 
  For this I created multiple sets by importing string and using functions like string.ascii_lowercase,string.digits,
  string.ascii_uppercase,string.punctuation .I stored these values in the string and used issubset function to check 
  whether the string password is subset of the the whole set.If yes,then the string lies in the particular complexity level.

c.For, the Create_User method retrieved a random user from "https://randomuser.me/api/" by using requests.get method on the api and 
  converted the retrieved data into  dictionary format, so that it would be easy to retrieve the name and email from the dictionary. 
  After which, did a connection with the Sqlite3 db and created table for fullname of user as well as email.Then inseted random values 
  fetched from the API into the table

2.The password_checker.py script euns for range 0 to 20 and generates random complexity level from 1-4 as well as password length from 4 to 20.
 Then these values are passed into generate password function present in Password_generator_main.If the complexity levels match 
 from the complexity levels generated in check_password_level functions then the scrip returns success otherwise it returns failure

The User_password_generator.py script runs from the range 0 to 20 and creates database connection with sqlite db .It fetches string fullname from create_user function present in Password_Generator_main and randomly generates password for the particular user by calling generate_password function.
Then in the end the password for the given user is updated using the update query.
